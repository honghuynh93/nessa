<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'nessa' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '4t=?ep,wwG,,d}M/]LV[[[}vQe$Wli;+V,L;pEdydYjQk%/&>znem70edlx+_-eR' );
define( 'SECURE_AUTH_KEY',  '}-#V3!6!ZD[UM~_p*NH8~f5&5x;h7k:0`{~*G{^.u~-pEKu}LlIc{M1O1YD%dr,>' );
define( 'LOGGED_IN_KEY',    'Q2[3<d;.VrTMj0;![W6tFWhHEq{-MMEE@$Rl5J[C}6BD`+{}WF:xy%*Y@ez Hif(' );
define( 'NONCE_KEY',        '=_?pFKA`!&,P*]#6jI%F2=Y+j+exH#dfZV@Z8@)dTUTgc4GP~5x-7[%no@ goQY0' );
define( 'AUTH_SALT',        '.7#SHS}+R%Rh`MU7lKB/6%_xGg8-28huAZS8[#~J3^t_JW`pV5R YZF2a:[0nk|l' );
define( 'SECURE_AUTH_SALT', 'f`tX?mX?wqtfY4PZo>V<O7g!n7g]?u4=xxxq]=&gPsX]Pl&|@h%OO,/+%1}dW#Mo' );
define( 'LOGGED_IN_SALT',   'jv+2@o|o-%<MY{<uM:.`xj#olO}&k/o]f_wfl^l:eIa =8n:Pg2*,,WR]NZTy/eV' );
define( 'NONCE_SALT',       'W]:F3y,!gF>Ck 3-3nA`+72;r%9]<3X0qY-|>[z(6$8_%`Ds9ey^NA( yM(#c&K.' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'ns_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
