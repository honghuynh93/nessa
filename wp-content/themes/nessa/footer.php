<footer class="footer">
    <div class="footer__content">
        <div class="row">
            <div class="col-12 col-md-6 col-xl-3">
                <div class="footer__item footer__logo">
                    <a href="/">
                        <img class="title" src="<?= get_template_directory_uri()?>/assets/img/positive.png" />
                    </a>
                    <p class="sub-title">Experienced doctors just a click away</p>
                </div>
            </div>
            <div class="col-12 col-md-6 col-xl-3">
                <div class="footer__item">
                    <p class="title">Company</p>
                    <ul>
                        <li><a href="/about/">About</a></li>
                        <li><a href="/collaborative/">Collaborative Care</a></li>
                        <li><a href="/telemedicine/">Telemedicine</a></li>
                        <li><a href="/insight/">Insight</a></li>
                        <li><a href="/contact/">Contact</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-12 col-md-6 col-xl-3">
                <div class="footer__item">
                    <p class="title">Get care</p>
                    <ul>
                        <li><a href="./">Free Symptom Checker</a></li>
                        <li><a href="./">Chat with a Doctor</a></li>
                        <li><a href="./">Collaborative Care Membership</a></li>
                        <li><a href="/faq/">FAQ</a></li>
                        <li><a href="./">Press Releases</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-12 col-md-6 col-xl-3">
                <div class="footer__item footer__contact">
                    <div>
                        <p class="title">Follow us</p>
                        <ul class="footer__icon d-flex justify-content-between">
                            <li><a href="./"><img src="<?= get_template_directory_uri()?>/assets/img/facebook.svg" alt="facebook" /></a></li>
                            <li><a href="./"><img src="<?= get_template_directory_uri()?>/assets/img/instagram.svg" alt="facebook" /></a></i></li>
                            <li><a href="./"><img src="<?= get_template_directory_uri()?>/assets/img/twitter.svg" alt="facebook" /></a></i></li>
                            <li><a href="./"><img src="<?= get_template_directory_uri()?>/assets/img/in.svg" alt="facebook" /></a></i></li>
                        </ul>
                    </div>
                    <ul class="footer__policy">
                        <li><a href="./">Terms</a></li>
                        <li><a href="./">Privacy</a></li>
                        <li><a href="./">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__bottom"></div>
</footer>
</div>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!--Slick -->
<script src="<?php echo get_template_directory_uri() ?>/assets/lib/slick/slick.min.js"></script>
<!-- Parsley -->
<script src="<?php echo get_template_directory_uri() ?>/assets/lib/parsley/parsley.min.js"></script>
<!-- Main Js -->
<script src="<?php echo get_template_directory_uri() ?>/assets/js/main.js"></script>
<?php wp_footer(); ?>
</body>

</html>