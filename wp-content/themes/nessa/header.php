<!doctype html>
<html lang="en" class="no-js" <?php language_attributes(); ?>>

<head>
  <title>Nessa</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/assets/css/main.css" />
  <!-- Font awesome -->
  <link rel="stylesheet" type="text/css"
    href="<?php echo get_template_directory_uri() ?>/assets/lib/font-awesome 5/css/all.css" />
  <!-- Slick -->
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/assets/lib/slick/slick.css" />
  <link rel="stylesheet" type="text/css"
    href="<?php echo get_template_directory_uri() ?>/assets/lib/slick/slick-theme.css" />
  <!-- Font family -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Fira+Sans:wght@700&display=swap" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <?php wp_head(); ?>

  <?php if ( is_admin_bar_showing() ): ?>
  <style>
  .header__content {
    margin-top: 32px;
  }

  html #wpadminbar {
    position: fixed !important;
  }

  .header-top-mobile {
    margin-top: 46px;
  }

  @media screen and (max-width: 782px) {
    html {
        margin-top: 0px !important;
    }
}
  </style>
  <?php endif?>
</head>

<body <?php body_class(); ?>>
  <header class="header d-none d-xl-block">
    <div class="header__content">
      <div class="header__logo header__left">
        <a href="/">
          <img src="<?php echo get_template_directory_uri()?>/assets/img/logo.png" alt="logo">
        </a>
      </div>
      <div class="header__right">
        <ul class="d-flex align-items-center">
          <li><a href="/about/">About</a></li>
          <li><a href="/collaborative/">Collaborative Care</a></li>
          <li><a href="/telemedicine/">Telemedicine</a></li>
          <li><a href="/insight/">Insight</a></li>
          <li><a href="/contact/">Contact</a></li>
          <?php if ( is_user_logged_in() ) { ?>
          <?php $current_user = get_userdata(get_current_user_id()); ?>
          <div class="header__sub-right">
            <ul class="d-flex align-items-center">
              <h5 class="mb-0">Hello, <span><?= $current_user->first_name . ' ' . $current_user->last_name ?></span>
              </h5>
              <a href="<?php echo wp_logout_url(); ?>">
                <button class="btn btn-color-pink btn-log-out">Log out</button>
              </a>
            </ul>
          </div>
          <?php } else { ?>
          <a href="/register/">
            <button class="btn btn-color-pink btn-sign-in">Get Started</button>
          </a>
          <a href="/login/">
            <li class="user-icon"><img src="<?php echo get_template_directory_uri()?>/assets/img/user.svg" alt="user" />
            </li>
          </a>
          <?php } ?>
        </ul>
      </div>
    </div>
  </header>

  <section class="header-mobile d-block d-xl-none">
    <div class="header-top-mobile navbar">
      <div class="burger-mobile">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
      </div>
      <div class="">
        <a href="/">
          <img src="<?php echo get_template_directory_uri()?>/assets/img/logo.png" alt="logo">
        </a>
      </div>

      <?php if ( is_user_logged_in() ) { ?>
      <a href="<?php echo wp_logout_url(); ?>" class="logout-icon">
        <i class="far fa-sign-out"></i>
      </a>
      <?php } else { ?>
      <a href="/login/" class="user-icon">
        <img src="<?php echo get_template_directory_uri()?>/assets/img/user-mobile.svg" alt="user" />
      </a>
      <?php } ?>
    </div>
    <div class="screen">
      <!-- <div class="navbar"></div> -->
      <!-- <div class="circle"></div> -->
      <div class="menu">
        <?php if ( is_user_logged_in() ) { ?>
        <img src="<?php echo get_template_directory_uri()?>/assets/img/user-mobile.svg" alt="user" class="user-icon" />
        <h5 class="mb-0">Hello, <span><?= $current_user->first_name . ' ' . $current_user->last_name ?></span></h5>
        <?php } ?>
        <ul>
          <li><a href="/about/">About</a></li>
          <li><a href="/collaborative/">Collaborative Care</a></li>
          <li><a href="/telemedicine/">Telemedicine</a></li>
          <li><a href="/insight/">Insight</a></li>
          <li><a href="/contact/">Contact</a></li>
          <?php if ( !is_user_logged_in() ) { ?>
          <a href="/register/">
            <button class="btn btn-color-pink btn-sign-in">Get Started</button>
          </a>
          <?php } ?>
        </ul>
      </div>
    </div>
  </section>