<?php
/**
 * Template Name: About Page
 * Template Post Type: page
 */

get_header();
?>

<div class="page-content">
    <section class="hero-section">
        <div class="hero-section-content container-vertical">
            <div class="hero-section-text">
                <p class="title-small">OUR STORY</p>
                <h2 class="title-white">Simplify healthcare for greater outreach</h2>
            </div>

        </div>
    </section>

    <section class="about__mission">
        <div class="about__mission-content container-vertical">
            <div class="about__mission-text">
                <p class="title-small">OUR MISSION</p>
                <P class="description-top">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et dolore
                </P>
                <P class="description-bottom">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                    aliquip ex ea commodo consequat. Duis auete irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur.
                </P>
            </div>
        </div>
    </section>


    <section class="about__mission-tab">
        <div class="about__mission-tab-content container-vertical">
            <div class="tab-name">
                <ul class="nav" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="values-tab" data-toggle="tab" href="#values" role="tab"
                            aria-controls="values" aria-selected="true">Values</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="philosophy-tab" data-toggle="tab" href="#philosophy" role="tab"
                            aria-controls="philosophy" aria-selected="false">Philosophy & Vision</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content">
                <div class="tab-pane tab-pane-values fase show active" id="values" role="tabpanel"
                    aria-labelledby="values-tab">
                    <div class="values-slide">
                        <div class="values-item">
                            <div class="title">
                                <p>Empathy</p>
                            </div>
                            <p class="description">
                                We care and understand the challenges for underserved patients on every visit.
                            </p>
                        </div>
                        <div class="values-item">
                            <div class="title">
                                <p>Cultural Sensitivity</p>
                            </div>
                            <p class="description">
                                We respect and care for the patient and understand cultural nuances to provide the most
                                inclusive service.
                            </p>
                        </div>
                        <div class="values-item">
                            <div class="title">
                                <p>Quality Providers</p>
                            </div>
                            <p class="description">
                                We only onboard doctors and translators who believe in exceptional quality care
                                regardless of gender, race, and age.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="tab-pane tab-pane-philosophy fase" id="philosophy" role="tabpanel"
                    aria-labelledby="philosophy-tab">
                    <div class="philosophy-content">
                        <div class="philosophy-item">
                            <h4 class="title">Our philosophy on today’s healthcare landscape</h4>
                            <p class="description">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
                                dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                            </p>
                        </div>

                        <div class="philosophy-item">
                            <h4 class="title">Our vision</h4>
                            <p class="description">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                exercitation ullamco laboris nisi ut aliquip.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="feeling">
        <div class="feeling__content d-flex justify-content-center align-items-center">
            <h2 class="feeling__title">How are you feeling today?</h2>
            <a href="/faq/" class="btn btn-border-white-large btn-chat">Chat with a Doctor</a>
        </div>
    </section>

    <section class="testimonials">
        <div class="testimonials__content container-vertical">
            <div class="testimonials__title text-center">
                <p class="title-small">TESTIMONIALS</p>
                <h2 class="title-large">So many people couldn't be wrong</h2>
            </div>
            <div class="testimonials__slide">
                <div class="testimonials__item">
                    <div class="testimonials__item-wrapper">
                        <div>
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 5.png" alt="quote" />
                        </div>
                        <div class="wrapper-description">
                            <p class="description">
                                "Donec quam nunc, accumsan sed enim viverra, semper ultrices nulla. Praesent vestibulum"
                            </p>
                            <p class="name">Bob G </p>
                        </div>
                    </div>
                </div>
                <div class="testimonials__item">
                    <div class="testimonials__item-wrapper">
                        <div>
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 5.png" alt="quote" />
                        </div>
                        <div class="wrapper-description">
                            <p class="description">
                                "Donec quam nunc, accumsan sed enim viverra, semper ultrices nulla. Praesent vestibulum"
                            </p>
                            <p class="name">Bob A</p>
                        </div>
                    </div>
                </div>
                <div class="testimonials__item">
                    <div class="testimonials__item-wrapper">
                        <div>
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 5.png" alt="quote" />
                        </div>
                        <div class="wrapper-description">
                            <p class="description">
                                "Donec quam nunc, accumsan sed enim viverra, semper ultrices nulla. Praesent vestibulum"
                            </p>
                            <p class="name">Bob B</p>
                        </div>
                    </div>
                </div>
                <div class="testimonials__item">
                    <div class="testimonials__item-wrapper">
                        <div>
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 5.png" alt="quote" />
                        </div>
                        <div class="wrapper-description">
                            <p class="description">
                                "Donec quam nunc, accumsan sed enim viverra, semper ultrices nulla. Praesent vestibulum"
                            </p>
                            <p class="name">Bob D</p>
                        </div>
                    </div>
                </div>
                <div class="testimonials__item">
                    <div class="testimonials__item-wrapper">
                        <div>
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 5.png" alt="quote" />
                        </div>
                        <div class="wrapper-description">
                            <p class="description">
                                "Donec quam nunc, accumsan sed enim viverra, semper ultrices nulla. Praesent vestibulum"
                            </p>
                            <p class="name">Bob E</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <a href="/reviews/" class="btn btn-border-purple-large">Reviews</a>
            </div>
        </div>
    </section>

    <section class="getstarted">
        <div class="getstarted__content text-center">
            <p class="title-small">TELEMEDICINE FOR ALL</p>
            <h3 class="title-white">Gain a highly experienced perspective today</h3>
            <a href="/register/" class="btn btn-border-white-large">Get Started</a>
        </div>
    </section>

    <section class="industry">
        <div class="industry__content container-vertical">
            <div class="industry__title text-center">
                <p class="title-small">INDUSTRY INSIGHT</p>
                <h2 class="title-large">Health that is real wealth</h2>
            </div>
            <div class="industry__slide">
                <div class="industry__item">
                    <div class="industry__wrapper">
                        <div>
                            <img class="w-100" src="<?php echo get_template_directory_uri()?>/assets/img/NoPath1.jpg" />
                        </div>
                        <p class="date">8.18.2020</p>
                        <div class="wrapper-description">
                            <h4 class="title">Medical Obesity</h4>
                            <p class="description">
                                I believe there is such a thing as Medical Obesity. Obesity is defined as a condition
                                characterized by the excessive accumulation and storage of fat in the body…
                            </p>
                            <a class="btn-read-more">READ MORE ></a>
                        </div>

                    </div>
                </div>
                <div class="industry__item">
                    <div class="industry__wrapper">
                        <div>
                            <img class="w-100" src="<?php echo get_template_directory_uri()?>/assets/img/NoPath2.jpg" />
                        </div>
                        <p class="date">8.18.2020</p>
                        <div class="wrapper-description">
                            <h4 class="title">On Medical Community</h4>
                            <p class="description">
                                Nowadays, we find a tsunami of articles on the epidemics of moral injury and physician
                                burnout. As physicians, we were trained to care for our patients. 99.9%…
                            </p>
                            <a class="btn-read-more">READ MORE ></a>
                        </div>
                    </div>
                </div>
                <div class="industry__item">
                    <div class="industry__wrapper">
                        <div>
                            <img class="w-100" src="<?php echo get_template_directory_uri()?>/assets/img/NoPath3.jpg" />
                        </div>
                        <p class="date">8.18.2020</p>
                        <div class="wrapper-description">
                            <h4 class="title">For The Love of Medicine</h4>
                            <p class="description">
                                This is my response to a recent Newsweek opinion piece written by a seemingly well-
                                respected Yale epidemiologist Dr. Harvey Risch. The SARS CoV2 virus…
                            </p>
                            <a class="btn-read-more">READ MORE ></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <a href="/insight/" class="btn btn-border-purple-large btn-read-more">Read More</a>
            </div>
        </div>
    </section>

</div>

<?php
get_footer();