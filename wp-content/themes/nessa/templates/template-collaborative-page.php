<?php
/**
 * Template Name: Collaborative Page
 * Template Post Type: page
 */

get_header();
?>

<div class="page-content">
    <section class="collaborative__hero-section">
        <div class="collaborative__hero-section-content container-vertical">
            <div class="collaborative__hero-section-text">
                <p class="title-small">
                    PERSONALIZED COLLABORATIVE CARE
                </p>
                <h2 class="title-white">
                    Our team is focused on a personalized approach, centered around you
                </h2>
                <p class="description">
                    We believe full spectrum healthcare is at it’s best, when it is collective and carefully coordinated
                    to achieve the best outcome for our patients.
                </p>
            </div>
        </div>
    </section>

    <section class="collaborative__telemedicine">
        <div class="collaborative__telemedicine-top">
            <h4>
                For less than $2 a day, here’s what it covers:
            </h4>
        </div>
        <div class="collaborative__telemedicine-content container-vertical">
            <div class="collaborative__telemedicine-benefit">

                <div class="row">
                    <div class="col-12 col-md-4">
                        <div class="collaborative__telemedicine-benefit-item">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/Group 2261.svg" />
                            <p class="title">Primary Care</p>
                            <p class="description">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua.
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="collaborative__telemedicine-benefit-item">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/Group 2265.svg" />
                            <p class="title">Health & Wellness</p>
                            <p class="description">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua.
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="collaborative__telemedicine-benefit-item">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/Group 2251.svg" />
                            <p class="title">Specialty Doctors</p>
                            <p class="description">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua.
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="collaborative__telemedicine-benefit-item">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/Group 2262.svg" />
                            <p class="title">Fitness & Nutrition</p>
                            <p class="description">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua.
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="collaborative__telemedicine-benefit-item">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/Group 2198.svg" />
                            <p class="title">Mental Health </p>
                            <p class="description">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua.
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="collaborative__telemedicine-benefit-item">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/Group 2199.svg" />
                            <p class="title">Acupuncture & More</p>
                            <p class="description">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="collaborative__telemedicine-timeline">
                <div class="headline-wrapper">
                    <h4 class="headline">
                        Unlimited Telemedicine Visits
                    </h4>
                </div>
                <div class="collaborative__telemedicine-timeline-item">
                    <div class="row">
                        <div class="col-12 col-md-6 order">
                            <div class="timeline-item-img">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/Group 2338.png" />
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="timeline-text">
                                <p class="title">STEP 1</p>
                                <h3 class="sub-title">
                                    Getting to know you & your health care needs
                                </h3>
                                <p class="description">
                                    The more we know about you, your medical history, lifestyle, genetics, and symptoms,
                                    the more we’ll understand you as a person. Your Nessa Team will take the time to get
                                    to know you.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="collaborative__telemedicine-timeline-item">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="timeline-text second">
                                <p class="title">STEP 2</p>
                                <h3 class="sub-title">
                                    Our 360° telehealth evaluation and personalized plan
                                </h3>
                                <p class="description">
                                    You’ll receive a tailored health plan, created by the Nessa Team. Which includes
                                    recommendations on next steps, nutrition, movement, life-style adjustments,
                                    medicine, supplements and more.
                                </p>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 order">
                            <div class="timeline-item-img">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/Group 2336.png" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="collaborative__telemedicine-timeline-item">
                    <div class="row">
                        <div class="col-12 col-md-6 order">
                            <div class="timeline-item-img">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/Group 2337.png" />
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="timeline-text">
                                <p class="title">STEP 3</p>
                                <h3 class="sub-title">
                                    Ongoing coordinated care with your primary physician and the Nessa Team
                                </h3>
                                <p class="description">
                                    Your needs are unique, and so are our personalized health plans. Our network of
                                    doctors will coordinate your needs, and work as a team for a wholistic approach to
                                    health care.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="collaborative__telemedicine-timeline-item">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="timeline-text second">
                                <p class="title">STEP 4</p>
                                <h3 class="sub-title">
                                    See the results & achieve your ultimate health goals
                                </h3>
                                <p class="description">
                                    We discuss health needs and goals throughout our process, and track your progress
                                    throughout. This ensures that you not only meet, but exceed all of your health
                                    expectations.
                                </p>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 order">
                            <div class="timeline-item-img">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/Group 2335.png" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="collaborative__telemedicine-bottom">
        </div>
    </section>

    <section class="group-section">
        <section class="collaborative__package">
            <div class="collaborative__package-content">
                <div class="text-center">
                    <div class="collaborative__package-title">
                        <h2 class="headline">Customize the care <br class="d-block d-md-none"> your family needs</h2>
                        <h4 class="title">Unlimited Telemedicine Visits</h4>
                        <div class="d-flex justify-content-center align-items-center">
                            <p class="unit mb-0">$</p>
                            <div class="d-flex align-items-end">
                                <div>
                                    <p class="mb-0 only">Only</p>
                                    <p class="price mb-0">49</p>
                                </div>
                                <p class="month">mo.</p>
                            </div>
                        </div>

                    </div>
                    <div class="collaborative__package-text">
                        <div class="collaborative__package-text-wrapper">
                            <div class="row">
                                <div class="col-12 col-md-6 collaborative__package-text-item-wrapper">
                                    <div class="collaborative__package-text-item d-flex align-items-baseline">
                                        <div class="check-wrapper">
                                            <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 3229.svg"
                                                alt="check" />
                                        </div>
                                        <div class="text-left">
                                            <p class="title">
                                                Unlimited Telemedicine Visits
                                            </p>
                                            <p class="description">
                                                In depth doctor visits let us get to know you and your health needs to
                                                personalize your care.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 collaborative__package-text-item-wrapper">
                                    <div class="collaborative__package-text-item d-flex align-items-baseline">
                                        <div class="check-wrapper">
                                            <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 3229.svg"
                                                alt="check" />
                                        </div>
                                        <div class="text-left">
                                            <p class="title">
                                                Coordinated Care Team
                                            </p>
                                            <p class="description">
                                                Your doctor and care manager work together with the Nessa Team to
                                                provide continuous support.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="collaborative__package-text-wrapper">
                            <div class="row">
                                <div class="col-12 col-md-6 collaborative__package-text-item-wrapper">
                                    <div class="collaborative__package-text-item d-flex align-items-baseline">
                                        <div class="check-wrapper">
                                            <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 3229.svg"
                                                alt="check" />
                                        </div>
                                        <div class="text-left">
                                            <p class="title">
                                                Unlimited Messaging
                                            </p>
                                            <p class="description">
                                                Your team is always here for you, connect with them anytime through our
                                                members dashboard.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 collaborative__package-text-item-wrapper">
                                    <div class="collaborative__package-text-item d-flex align-items-baseline">
                                        <div class="check-wrapper">
                                            <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 3229.svg"
                                                alt="check" />
                                        </div>
                                        <div class="text-left">
                                            <p class="title">
                                                Personalized Health Plan
                                            </p>
                                            <p class="description">
                                                Your unique health plan covers nutrition, movement, sleep and more to
                                                help you achieve better health.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="collaborative__package-text-wrapper">
                            <div class="row">
                                <div class="col-12 col-md-6 collaborative__package-text-item-wrapper">
                                    <div class="collaborative__package-text-item d-flex align-items-baseline">
                                        <div class="check-wrapper">
                                            <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 3229.svg"
                                                alt="check" />
                                        </div>
                                        <div class="text-left">
                                            <p class="title">
                                                Ongoing Support
                                            </p>
                                            <p class="description">
                                                Your team is by your side throughout your health journey for
                                                recommendations, or to answer any questions.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 collaborative__package-text-item-wrapper">
                                    <div class="collaborative__package-text-item d-flex align-items-baseline">
                                        <div class="check-wrapper">
                                            <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 3229.svg"
                                                alt="check" />
                                        </div>
                                        <div class="text-left">
                                            <p class="title">
                                                Membership Perks
                                            </p>
                                            <p class="description">
                                                Enjoy preferred pricing on offers from our health and wellness partners.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="/register/" class="btn btn-color-orange">Join Now</a>
                </div>
            </div>
        </section>

        <section class="testimonials">
            <div class="testimonials__content container-vertical">
                <div class="testimonials__title text-center">
                    <p class="title-small">TESTIMONIALS</p>
                    <h2 class="title-large">So many people couldn't be wrong</h2>
                </div>
                <div class="testimonials__slide">
                    <div class="testimonials__item">
                        <div class="testimonials__item-wrapper">
                            <div>
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 5.png"
                                    alt="quote" />
                            </div>
                            <div class="wrapper-description">
                                <p class="description">
                                    "Donec quam nunc, accumsan sed enim viverra, semper ultrices nulla. Praesent
                                    vestibulum"
                                </p>
                                <p class="name">Bob G </p>
                            </div>
                        </div>
                    </div>
                    <div class="testimonials__item">
                        <div class="testimonials__item-wrapper">
                            <div>
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 5.png"
                                    alt="quote" />
                            </div>
                            <div class="wrapper-description">
                                <p class="description">
                                    "Donec quam nunc, accumsan sed enim viverra, semper ultrices nulla. Praesent
                                    vestibulum"
                                </p>
                                <p class="name">Bob A</p>
                            </div>
                        </div>
                    </div>
                    <div class="testimonials__item">
                        <div class="testimonials__item-wrapper">
                            <div>
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 5.png"
                                    alt="quote" />
                            </div>
                            <div class="wrapper-description">
                                <p class="description">
                                    "Donec quam nunc, accumsan sed enim viverra, semper ultrices nulla. Praesent
                                    vestibulum"
                                </p>
                                <p class="name">Bob B</p>
                            </div>
                        </div>
                    </div>
                    <div class="testimonials__item">
                        <div class="testimonials__item-wrapper">
                            <div>
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 5.png"
                                    alt="quote" />
                            </div>
                            <div class="wrapper-description">
                                <p class="description">
                                    "Donec quam nunc, accumsan sed enim viverra, semper ultrices nulla. Praesent
                                    vestibulum"
                                </p>
                                <p class="name">Bob D</p>
                            </div>
                        </div>
                    </div>
                    <div class="testimonials__item">
                        <div class="testimonials__item-wrapper">
                            <div>
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 5.png"
                                    alt="quote" />
                            </div>
                            <div class="wrapper-description">
                                <p class="description">
                                    "Donec quam nunc, accumsan sed enim viverra, semper ultrices nulla. Praesent
                                    vestibulum"
                                </p>
                                <p class="name">Bob E</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <a href="/reviews/" class="btn btn-border-purple-large">Reviews</a>
                </div>
            </div>
        </section>
    </section>


    <section class="feeling">
        <div class="feeling__content d-flex justify-content-center align-items-center">
            <h2 class="feeling__title">How are you feeling today?</h2>
            <a href="/faq/" class="btn btn-border-white-large btn-chat">Chat with a Doctor</a>
        </div>
    </section>

    
    <section class="questions">
        <div class="questions__title">
            <p class="title-small">FREQUENTLY ASKED QUESTIONS</p>
            <h2 class="title-large">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</h2>
            <div class="tab-name">
                <ul class="nav" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="general-tab" data-toggle="tab" href="#general" role="tab"
                            aria-controls="general" aria-selected="true">
                            General Info
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="everyday-tab" data-toggle="tab" href="#everyday" role="tab"
                            aria-controls="everyday" aria-selected="false">
                            Everyday Care
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="mental-tab" data-toggle="tab" href="#mental" role="tab"
                            aria-controls="mental" aria-selected="false">
                            Mental Health
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="prescriptions-tab" data-toggle="tab" href="#prescriptions" role="tab"
                            aria-controls="prescriptions" aria-selected="false">
                            Prescriptions
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="questions__content container-vertical text-center">
            <div class="tab-content">
                <div class="tab-pane tab-pane-general fade show active" id="general" role="tabpanel"
                    aria-labelledby="general-tab">
                    <div class="questions__wrapper" id="accordion">
                        <div class="questions__item d-flex justify-content-between align-items-center"
                            id="questions__itemOne">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I qualify for NessaMD?</p>
                            </div>
                            <i class="fas fa-plus" data-toggle="collapse" data-target="#collapseOne"
                                aria-expanded="true" aria-controls="collapseOne"></i>
                        </div>
                        <div id="collapseOne" class="collapse collapse-content" aria-labelledby="questions__itemOne"
                            data-parent="#accordion">
                            <div class="collapse-text">
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias debitis expedita
                                quis? Dolorem voluptates doloremque iure fuga, nisi ipsam voluptatibus exercitationem
                                excepturi dignissimos cum labore omnis dolorum velit perferendis, facilis optio ipsa
                                consectetur fugit quos vero sint minima eum! Dignissimos ab provident earum vel, sequi
                                quo at? Quo odio quaerat in repellat cumque totam nam quas, velit tenetur officia
                                voluptatem. Obcaecati, officiis! Cum fuga, odit, in minus suscipit voluptatem voluptates
                                quas nihil, nam eaque nesciunt officiis. Velit, nulla autem aliquid nobis accusamus
                                cupiditate inventore magni aperiam earum totam harum ducimus commodi similique vero
                                ipsam quos voluptate, qui eius? Hic, sequi?
                            </div>
                        </div>

                        <div class="questions__item d-flex justify-content-between align-items-center"
                            id="questions__itemTwo">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>Will I speak with a real doctor?</p>
                            </div>
                            <i class="fas fa-plus" data-toggle="collapse" data-target="#collapseTwo"
                                aria-expanded="true" aria-controls="collapseTwo"></i>
                        </div>
                        <div id="collapseTwo" class="collapse collapse-content" aria-labelledby="questions__itemTwo"
                            data-parent="#accordion">
                            <div class="collapse-text">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam sequi perspiciatis, libero
                                modi esse non iste dolorum eos praesentium amet deleniti facere sunt, aperiam placeat
                                voluptatum quos consequuntur dolore debitis!
                            </div>
                        </div>

                        <div class="questions__item d-flex justify-content-between align-items-center"
                            id="questions__itemThree">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>Can NessaMD assist with emergency situations?</p>
                            </div>
                            <i class="fas fa-plus" data-toggle="collapse" data-target="#collapseThree"
                                aria-expanded="true" aria-controls="collapseThree"></i>
                        </div>
                        <div id="collapseThree" class="collapse collapse-content" aria-labelledby="questions__itemThree"
                            data-parent="#accordion">
                            <div class="collapse-text">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam sequi perspiciatis, libero
                                modi esse non iste dolorum eos praesentium amet deleniti facere sunt, aperiam placeat
                                voluptatum quos consequuntur dolore debitis!
                            </div>
                        </div>

                        <div class="questions__item d-flex justify-content-between align-items-center"
                            id="questions__itemFour">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>Can I request a specific doctor?</p>
                            </div>
                            <i class="fas fa-plus" data-toggle="collapse" data-target="#collapseFour"
                                aria-expanded="true" aria-controls="collapseFour"></i>
                        </div>
                        <div id="collapseFour" class="collapse collapse-content" aria-labelledby="questions__itemFour"
                            data-parent="#accordion">
                            <div class="collapse-text">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam sequi perspiciatis, libero
                                modi esse non iste dolorum eos praesentium amet deleniti facere sunt, aperiam placeat
                                voluptatum quos consequuntur dolore debitis!
                            </div>
                        </div>

                        <div class="questions__item d-flex justify-content-between align-items-center"
                            id="questions__itemFive">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I register for a new account?</p>
                            </div>
                            <i class="fas fa-plus" data-toggle="collapse" data-target="#collapseFive"
                                aria-expanded="true" aria-controls="collapseFive"></i>
                        </div>
                        <div id="collapseFive" class="collapse collapse-content" aria-labelledby="questions__itemFive"
                            data-parent="#accordion">
                            <div class="collapse-text">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam sequi perspiciatis, libero
                                modi esse non iste dolorum eos praesentium amet deleniti facere sunt, aperiam placeat
                                voluptatum quos consequuntur dolore debitis!
                            </div>
                        </div>

                        <div class="questions__item d-flex justify-content-between align-items-center"
                            id="questions__itemSix">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I add dependents to my account?</p>
                            </div>
                            <i class="fas fa-plus" data-toggle="collapse" data-target="#collapseSix"
                                aria-expanded="true" aria-controls="collapseSix"></i>
                        </div>
                        <div id="collapseSix" class="collapse collapse-content" aria-labelledby="questions__itemSix"
                            data-parent="#accordion">
                            <div class="collapse-text">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam sequi perspiciatis, libero
                                modi esse non iste dolorum eos praesentium amet deleniti facere sunt, aperiam placeat
                                voluptatum quos consequuntur dolore debitis!
                            </div>
                        </div>

                        <div class="questions__item d-flex justify-content-between align-items-center"
                            id="questions__itemSeven">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I reset my password?</p>
                            </div>
                            <i class="fas fa-plus" data-toggle="collapse" data-target="#collapseSeven"
                                aria-expanded="true" aria-controls="collapseSeven"></i>
                        </div>
                        <div id="collapseSeven" class="collapse collapse-content" aria-labelledby="questions__itemSeven"
                            data-parent="#accordion">
                            <div class="collapse-text">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam sequi perspiciatis, libero
                                modi esse non iste dolorum eos praesentium amet deleniti facere sunt, aperiam placeat
                                voluptatum quos consequuntur dolore debitis!
                            </div>
                        </div>

                        <div class="questions__item d-flex justify-content-between align-items-center"
                            id="questions__itemEight">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I unlock my account?</p>
                            </div>
                            <i class="fas fa-plus" data-toggle="collapse" data-target="#collapseEight"
                                aria-expanded="true" aria-controls="collapseEight"></i>
                        </div>
                        <div id="collapseEight" class="collapse collapse-content" aria-labelledby="questions__itemEight"
                            data-parent="#accordion">
                            <div class="collapse-text">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam sequi perspiciatis, libero
                                modi esse non iste dolorum eos praesentium amet deleniti facere sunt, aperiam placeat
                                voluptatum quos consequuntur dolore debitis!
                            </div>
                        </div>

                        <div class="questions__item d-flex justify-content-between align-items-center"
                            id="questions__itemNine">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>I have not received a call from my provider.</p>
                            </div>
                            <i class="fas fa-plus" data-toggle="collapse" data-target="#collapseNine"
                                aria-expanded="true" aria-controls="collapseNine"></i>
                        </div>
                        <div id="collapseNine" class="collapse collapse-content" aria-labelledby="questions__itemNine"
                            data-parent="#accordion">
                            <div class="collapse-text">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam sequi perspiciatis, libero
                                modi esse non iste dolorum eos praesentium amet deleniti facere sunt, aperiam placeat
                                voluptatum quos consequuntur dolore debitis!
                            </div>
                        </div>

                        <div class="questions__item d-flex justify-content-between align-items-center"
                            id="questions__itemTen">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I request a refund??</p>
                            </div>
                            <i class="fas fa-plus" data-toggle="collapse" data-target="#collapseTen"
                                aria-expanded="true" aria-controls="collapseTen"></i>
                        </div>
                        <div id="collapseTen" class="collapse collapse-content" aria-labelledby="questions__itemTen"
                            data-parent="#accordion">
                            <div class="collapse-text">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam sequi perspiciatis, libero
                                modi esse non iste dolorum eos praesentium amet deleniti facere sunt, aperiam placeat
                                voluptatum quos consequuntur dolore debitis!
                            </div>
                        </div>

                        <div class="questions__item d-flex justify-content-between align-items-center"
                            id="questions__itemEleven">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I request a refund??</p>
                            </div>
                            <i class="fas fa-plus" data-toggle="collapse" data-target="#collapseEleven"
                                aria-expanded="true" aria-controls="collapseEleven"></i>
                        </div>
                        <div id="collapseEleven" class="collapse collapse-content"
                            aria-labelledby="questions__itemEleven" data-parent="#accordion">
                            <div class="collapse-text">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam sequi perspiciatis, libero
                                modi esse non iste dolorum eos praesentium amet deleniti facere sunt, aperiam placeat
                                voluptatum quos consequuntur dolore debitis!
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane tab-pane-everyday fade" id="everyday" role="tabpanel"
                    aria-labelledby="everyday-tab">
                    <div class="questions__wrapper">
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I qualify for NessaMD?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>Will I speak with a real doctor?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>Can NessaMD assist with emergency situations?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>Can I request a specific doctor?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                    </div>
                </div>
                <div class="tab-pane tab-pane-mental fade" id="mental" role="tabpanel" aria-labelledby="mental-tab">
                    <div class="questions__wrapper">
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>Can I request a specific doctor?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I register for a new account?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I add dependents to my account?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I reset my password?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I unlock my account?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>I have not received a call from my provider.</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I request a refund??</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I request a refund??</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                    </div>
                </div>
                <div class="tab-pane tab-pane-prescriptions fade" id="prescriptions" role="tabpanel"
                    aria-labelledby="prescriptions-tab">
                    <div class="questions__wrapper">
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>Can NessaMD assist with emergency situations?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>Can I request a specific doctor?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I register for a new account?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I add dependents to my account?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I reset my password?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I unlock my account?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="getstarted">
        <div class="getstarted__content text-center">
            <p class="title-small">TELEMEDICINE FOR ALL</p>
            <h3 class="title-white">Gain a highly experienced perspective today</h3>
            <a href="/register/" class="btn btn-border-white-large">Get Started</a>
        </div>
    </section>

    <section class="industry">
        <div class="industry__content container-vertical">
            <div class="industry__title text-center">
                <p class="title-small">INDUSTRY INSIGHT</p>
                <h2 class="title-large">Health that is real wealth</h2>
            </div>
            <div class="industry__slide">
                <div class="industry__item">
                    <div class="industry__wrapper">
                        <div>
                            <img class="w-100" src="<?php echo get_template_directory_uri()?>/assets/img/NoPath1.jpg" />
                        </div>
                        <p class="date">8.18.2020</p>
                        <div class="wrapper-description">
                            <h4 class="title">Medical Obesity</h4>
                            <p class="description">
                                I believe there is such a thing as Medical Obesity. Obesity is defined as a condition
                                characterized by the excessive accumulation and storage of fat in the body…
                            </p>
                            <a class="btn-read-more">READ MORE ></a>
                        </div>

                    </div>
                </div>
                <div class="industry__item">
                    <div class="industry__wrapper">
                        <div>
                            <img class="w-100" src="<?php echo get_template_directory_uri()?>/assets/img/NoPath2.jpg" />
                        </div>
                        <p class="date">8.18.2020</p>
                        <div class="wrapper-description">
                            <h4 class="title">On Medical Community</h4>
                            <p class="description">
                                Nowadays, we find a tsunami of articles on the epidemics of moral injury and physician
                                burnout. As physicians, we were trained to care for our patients. 99.9%…
                            </p>
                            <a class="btn-read-more">READ MORE ></a>
                        </div>
                    </div>
                </div>
                <div class="industry__item">
                    <div class="industry__wrapper">
                        <div>
                            <img class="w-100" src="<?php echo get_template_directory_uri()?>/assets/img/NoPath3.jpg" />
                        </div>
                        <p class="date">8.18.2020</p>
                        <div class="wrapper-description">
                            <h4 class="title">For The Love of Medicine</h4>
                            <p class="description">
                                This is my response to a recent Newsweek opinion piece written by a seemingly well-
                                respected Yale epidemiologist Dr. Harvey Risch. The SARS CoV2 virus…
                            </p>
                            <a class="btn-read-more">READ MORE ></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <a href="/insight/" class="btn btn-border-purple-large btn-read-more">Read More</a>
            </div>
        </div>
    </section>
</div>

<?php
get_footer();