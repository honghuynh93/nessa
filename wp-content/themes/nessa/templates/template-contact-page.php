<?php
/**
 * Template Name: Contact Page
 * Template Post Type: page
 */

get_header();
?>

<div class="page-content" id='contact'>
    <section class="hero-section">
        <div class="hero-section-content container-vertical">
            <div class="hero-section-text">
                <p class="title-small">CONTACT US</p>
                <h2 class="title-white-large">
                    We’ve made improving your health easier
                </h2>
            </div>
        </div>
    </section>

    <section class="contact">
        <div class="contact__content">
            <div class="row">
                <div class="col-12 col-md-5 col-lg-4 col-xl-3">
                    <div class="contact__left">
                        <h6 class="title">GENERAL INQUIRES</h6>
                        <div class="contact__email">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/0tubnA.svg" />
                            <span>info@nessamd.com</span>
                        </div>
                        <div class="d-flex align-items-baseline info contact__phone">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/246xoz.svg" />
                            <div>
                                <span>1-800-123-4567</span>
                                <p class="mb-0">8am - 5pm PST</p>
                            </div>
                        </div>
                        <div class="d-flex align-items-baseline info contact__chat">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/gosFMt.svg" />
                            <div>
                                <span>Live Chat!</span>
                                <p class="mb-0">8am - 5pm PST</p>
                            </div>
                        </div>
                        <div class="contact__partner">
                            <h6 class="title">PARTNERSHIP</h6>
                            <p>partnerships@nessamd.com</p>
                        </div>
                        <div class="contact__sales">
                            <h6 class="title">SALES</h6>
                            <p>sales@nessamd.com</p>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-7 col-lg-8 col-xl-9">
                    <div class="contact__right">
                        <p class="title">Send us a message.</p>
                        <p class="sub-title">We are here to help. Please fill in the fields below:</p>
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
                            the_content();
                        endwhile; else: ?>

                    <?php endif; ?>
                </div>
            </div>
           
        </div>
    </div>
</section>

<section class="feeling">
    <div class="feeling__content d-flex justify-content-center align-items-center">
        <h2 class="feeling__title">How are you feeling today?</h2>
        <a href="/faq/" class="btn btn-border-white-large btn-chat">Chat with a Doctor</a>
    </div>
</section>

<section class="industry">
    <div class="industry__content container-vertical">
        <div class="industry__title text-center">
            <p class="title-small">INDUSTRY INSIGHT</p>
            <h2 class="title-large">Health that is real wealth</h2>
        </div>
        <div class="industry__slide">
            <div class="industry__item">
                <div class="industry__wrapper">
                    <div>
                        <img class="w-100" src="<?php echo get_template_directory_uri()?>/assets/img/NoPath1.jpg" />
                    </div>
                    <p class="date">8.18.2020</p>
                    <div class="wrapper-description">
                        <h4 class="title">Medical Obesity</h4>
                        <p class="description">
                            I believe there is such a thing as Medical Obesity. Obesity is defined as a condition
                            characterized by the excessive accumulation and storage of fat in the body…
                        </p>
                        <a class="btn-read-more">READ MORE ></a>
                    </div>

                </div>
            </div>
            <div class="industry__item">
                <div class="industry__wrapper">
                    <div>
                        <img class="w-100" src="<?php echo get_template_directory_uri()?>/assets/img/NoPath2.jpg" />
                    </div>
                    <p class="date">8.18.2020</p>
                    <div class="wrapper-description">
                        <h4 class="title">On Medical Community</h4>
                        <p class="description">
                            Nowadays, we find a tsunami of articles on the epidemics of moral injury and physician
                            burnout. As physicians, we were trained to care for our patients. 99.9%…
                        </p>
                        <a class="btn-read-more">READ MORE ></a>
                    </div>
                </div>
            </div>
            <div class="industry__item">
                <div class="industry__wrapper">
                    <div>
                        <img class="w-100" src="<?php echo get_template_directory_uri()?>/assets/img/NoPath3.jpg" />
                    </div>
                    <p class="date">8.18.2020</p>
                    <div class="wrapper-description">
                        <h4 class="title">For The Love of Medicine</h4>
                        <p class="description">
                            This is my response to a recent Newsweek opinion piece written by a seemingly well-
                            respected Yale epidemiologist Dr. Harvey Risch. The SARS CoV2 virus…
                        </p>
                        <a class="btn-read-more">READ MORE ></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center">
            <a href="/insight/" class="btn btn-border-purple-large btn-read-more">Read More</a>
        </div>
    </div>
</section>
</div>

<?php
get_footer();