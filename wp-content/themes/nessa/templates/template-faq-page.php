<?php
/**
 * Template Name: FAQ Page
 * Template Post Type: page
 */

get_header();
?>

<div class="page-content">

    <section class="questions">
        <div class="questions__title">
            <p class="title-small">FREQUENTLY ASKED QUESTIONS</p>
            <h2 class="title-large">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</h2>
            <div class="tab-name">
                <ul class="nav" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="general-tab" data-toggle="tab" href="#general" role="tab"
                            aria-controls="general" aria-selected="true">
                            General Info
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="everyday-tab" data-toggle="tab" href="#everyday" role="tab"
                            aria-controls="everyday" aria-selected="false">
                            Everyday Care
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="mental-tab" data-toggle="tab" href="#mental" role="tab"
                            aria-controls="mental" aria-selected="false">
                            Mental Health
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="prescriptions-tab" data-toggle="tab" href="#prescriptions" role="tab"
                            aria-controls="prescriptions" aria-selected="false">
                            Prescriptions
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="questions__content container-vertical text-center">
            <div class="tab-content">
                <div class="tab-pane tab-pane-general fade show active" id="general" role="tabpanel"
                    aria-labelledby="general-tab">
                    <div class="questions__wrapper">
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I qualify for NessaMD?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>Will I speak with a real doctor?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>Can NessaMD assist with emergency situations?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>Can I request a specific doctor?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I register for a new account?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I add dependents to my account?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I reset my password?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I unlock my account?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>I have not received a call from my provider.</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I request a refund??</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I request a refund??</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                    </div>
                </div>
                <div class="tab-pane tab-pane-everyday fade" id="everyday" role="tabpanel"
                    aria-labelledby="everyday-tab">
                    <div class="questions__wrapper">
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I qualify for NessaMD?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>Will I speak with a real doctor?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>Can NessaMD assist with emergency situations?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>Can I request a specific doctor?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                    </div>
                </div>
                <div class="tab-pane tab-pane-mental fade" id="mental" role="tabpanel" aria-labelledby="mental-tab">
                    <div class="questions__wrapper">
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>Can I request a specific doctor?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I register for a new account?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I add dependents to my account?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I reset my password?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I unlock my account?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>I have not received a call from my provider.</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I request a refund??</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I request a refund??</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                    </div>
                </div>
                <div class="tab-pane tab-pane-prescriptions fade" id="prescriptions" role="tabpanel"
                    aria-labelledby="prescriptions-tab">
                    <div class="questions__wrapper">
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>Can NessaMD assist with emergency situations?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>Can I request a specific doctor?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I register for a new account?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I add dependents to my account?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I reset my password?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                        <div class="questions__item d-flex justify-content-between align-items-center">
                            <div class="questions__item-text d-flex align-items-center">
                                <p>How do I unlock my account?</p>
                            </div>
                            <i class="fas fa-plus"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>

<?php
get_footer();