<?php
/**
 * Template Name: Insight Page
 * Template Post Type: page
 */

get_header();
?>

<div class="page-content" id="insight">
    <section class="hero-section">
        <div class="hero-section-content container-vertical">
            <div class="hero-section-text">
                <p class="title-small">INDUSTRY INSIGHT</p>
                <h2 class="title-white-large">
                    Bridging the gap in healthcare connections
                </h2>
            </div>
        </div>
    </section>

    <section class="category">
        <div class="category__content">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-7 col-xl-9">
                    <div class="category__left">
                        <div class="category__item">
                            <div class="row">
                                <div class="col-12 col-md-4 col-lg-12 col-xl-4">
                                    <div class="category__img">
                                        <img class="w-100" src="<?php echo get_template_directory_uri()?>/assets/img/NoPath1.jpg" />
                                    </div>
                                </div>
                                <div class="col-12 col-md-8 col-lg-12 col-xl-8">
                                    <div class="category__text">
                                        <div class="d-flex justify-content-between">
                                            <p class="title">CATEGORY</p>
                                            <p class="date">8.18.2020</p>
                                        </div>
                                        <p class="sub-title">Medical Obesity</p>
                                        <p class="description">
                                            I believe there is such a thing as Medical Obesity. Obesity is defined as a
                                            condition characterized by the excessive accumulation and storage of fat in
                                            the body…
                                        </p>
                                        <div class="d-flex justify-content-between">
                                            <a href="" class="btn-read-more">READ MORE ></a>
                                            <div class="group-img">
                                                <a href=""><img src="<?php echo get_template_directory_uri()?>/assets/img/Group 500.svg" /></a>
                                                <a href=""><img src="<?php echo get_template_directory_uri()?>/assets/img/Group 498.svg" /></a>
                                                <a href=""><img src="<?php echo get_template_directory_uri()?>/assets/img/Group 497.svg" /></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="category__item">
                            <div class="row">
                                <div class="col-12 col-md-4 col-lg-12 col-xl-4">
                                    <div class="category__img">
                                        <img class="w-100" src="<?php echo get_template_directory_uri()?>/assets/img/NoPath2.jpg" />
                                    </div>
                                </div>
                                <div class="col-12 col-md-8 col-lg-12 col-xl-8">
                                    <div class="category__text">
                                        <div class="d-flex justify-content-between">
                                            <p class="title">CATEGORY</p>
                                            <p class="date">8.18.2020</p>
                                        </div>
                                        <p class="sub-title">On Medical Community</p>
                                        <p class="description">
                                            Nowadays, we find a tsunami of articles on the epidemics of moral injury and
                                            physician burnout. As physicians, we were trained to care for our patients.
                                            99.9%…
                                        </p>
                                        <div class="d-flex justify-content-between">
                                            <a href="" class="btn-read-more">READ MORE ></a>
                                            <div class="group-img">
                                                <a href=""><img src="<?php echo get_template_directory_uri()?>/assets/img/Group 500.svg" /></a>
                                                <a href=""><img src="<?php echo get_template_directory_uri()?>/assets/img/Group 498.svg" /></a>
                                                <a href=""><img src="<?php echo get_template_directory_uri()?>/assets/img/Group 497.svg" /></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="category__item">
                            <div class="row">
                                <div class="col-12 col-md-4 col-lg-12 col-xl-4">
                                    <div class="category__img">
                                        <img class="w-100" src="<?php echo get_template_directory_uri()?>/assets/img/NoPath3.jpg" />
                                    </div>
                                </div>
                                <div class="col-12 col-md-8 col-lg-12 col-xl-8">
                                    <div class="category__text">
                                        <div class="d-flex justify-content-between">
                                            <p class="title">CATEGORY</p>
                                            <p class="date">8.18.2020</p>
                                        </div>
                                        <p class="sub-title">For The Love of Medicine</p>
                                        <p class="description">
                                            This is my response to a recent Newsweek opinion piece written by a
                                            seemingly well- respected Yale epidemiologist Dr. Harvey Risch. The SARS
                                            CoV2 virus…
                                        </p>
                                        <div class="d-flex justify-content-between">
                                            <a href="" class="btn-read-more">READ MORE ></a>
                                            <div class="group-img">
                                                <a href=""><img src="<?php echo get_template_directory_uri()?>/assets/img/Group 500.svg" /></a>
                                                <a href=""><img src="<?php echo get_template_directory_uri()?>/assets/img/Group 498.svg" /></a>
                                                <a href=""><img src="<?php echo get_template_directory_uri()?>/assets/img/Group 497.svg" /></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="category__item">
                            <div class="row">
                                <div class="col-12 col-md-4 col-lg-12 col-xl-4">
                                    <div class="category__img">
                                        <img class="w-100" src="<?php echo get_template_directory_uri()?>/assets/img/NoPath1.jpg" />
                                    </div>
                                </div>
                                <div class="col-12 col-md-8 col-lg-12 col-xl-8">
                                    <div class="category__text">
                                        <div class="d-flex justify-content-between">
                                            <p class="title">CATEGORY</p>
                                            <p class="date">8.18.2020</p>
                                        </div>
                                        <p class="sub-title">Medical Obesity</p>
                                        <p class="description">
                                            I believe there is such a thing as Medical Obesity. Obesity is defined as a
                                            condition characterized by the excessive accumulation and storage of fat in
                                            the body…
                                        </p>
                                        <div class="d-flex justify-content-between">
                                            <a href="" class="btn-read-more">READ MORE ></a>
                                            <div class="group-img">
                                                <a href=""><img src="<?php echo get_template_directory_uri()?>/assets/img/Group 500.svg" /></a>
                                                <a href=""><img src="<?php echo get_template_directory_uri()?>/assets/img/Group 498.svg" /></a>
                                                <a href=""><img src="<?php echo get_template_directory_uri()?>/assets/img/Group 497.svg" /></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="category__item">
                            <div class="row">
                                <div class="col-12 col-md-4 col-lg-12 col-xl-4">
                                    <div class="category__img">
                                        <img class="w-100" src="<?php echo get_template_directory_uri()?>/assets/img/NoPath2.jpg" />
                                    </div>
                                </div>
                                <div class="col-12 col-md-8 col-lg-12 col-xl-8">
                                    <div class="category__text">
                                        <div class="d-flex justify-content-between">
                                            <p class="title">CATEGORY</p>
                                            <p class="date">8.18.2020</p>
                                        </div>
                                        <p class="sub-title">On Medical Community</p>
                                        <p class="description">
                                            Nowadays, we find a tsunami of articles on the epidemics of moral injury and
                                            physician burnout. As physicians, we were trained to care for our patients.
                                            99.9%…
                                        </p>
                                        <div class="d-flex justify-content-between">
                                            <a href="" class="btn-read-more">READ MORE ></a>
                                            <div class="group-img">
                                                <a href=""><img src="<?php echo get_template_directory_uri()?>/assets/img/Group 500.svg" /></a>
                                                <a href=""><img src="<?php echo get_template_directory_uri()?>/assets/img/Group 498.svg" /></a>
                                                <a href=""><img src="<?php echo get_template_directory_uri()?>/assets/img/Group 497.svg" /></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="category__item">
                            <div class="row">
                                <div class="col-12 col-md-4 col-lg-12 col-xl-4">
                                    <div class="category__img">
                                        <img class="w-100" src="<?php echo get_template_directory_uri()?>/assets/img/NoPath3.jpg" />
                                    </div>
                                </div>
                                <div class="col-12 col-md-8 col-lg-12 col-xl-8">
                                    <div class="category__text">
                                        <div class="d-flex justify-content-between">
                                            <p class="title">CATEGORY</p>
                                            <p class="date">8.18.2020</p>
                                        </div>
                                        <p class="sub-title">For The Love of Medicine</p>
                                        <p class="description">
                                            This is my response to a recent Newsweek opinion piece written by a
                                            seemingly well- respected Yale epidemiologist Dr. Harvey Risch. The SARS
                                            CoV2 virus…
                                        </p>
                                        <div class="d-flex justify-content-between">
                                            <a href="" class="btn-read-more">READ MORE ></a>
                                            <div class="group-img">
                                                <a href=""><img src="<?php echo get_template_directory_uri()?>/assets/img/Group 500.svg" /></a>
                                                <a href=""><img src="<?php echo get_template_directory_uri()?>/assets/img/Group 498.svg" /></a>
                                                <a href=""><img src="<?php echo get_template_directory_uri()?>/assets/img/Group 497.svg" /></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-lg-5 col-xl-3">
                    <div class="category__right">
                        <div class="category__search">
                            <input type="text" placeholder="Search our blog">
                            <button class="wrapper-img">
                                <img src="<?php echo get_template_directory_uri()?>/assets/img/Union 1.svg" />
                            </button>
                        </div>

                        <div class="category__email">
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/positive.svg" />
                            <p class="title">Healthcare without walls, sign up our newsletter</p>
                            <div class='category__email-input d-flex'>
                                <input placeholder="Enter Email" />
                                <button class="btn">GO</button>
                            </div>
                        </div>

                        <div class="category__tags">
                            <h5 class="title">Tags</h5>
                            <ul>
                                <li><a href="">Pain</a></li>
                                <li><a href="">Diagnosis</a></li>
                                <li><a href="">Obesity</a></li>
                                <li><a href="">Condition</a></li>
                                <li><a href="">Healthcare</a></li>
                                <li><a href="">Telehealth</a></li>
                                <li><a href="">Fat</a></li>
                                <li><a href="">Surgery</a></li>
                                <li><a href="">Misdiagnosis</a></li>
                            </ul>
                        </div>

                        <div class="category__menu">
                            <h5 class="title">Categories</h5>
                            <ul>
                                <li><a href="">Abdominal pain</a></li>
                                <li><a href="">Back and chest pain</a></li>
                                <li><a href="">Chronic disease</a></li>
                                <li><a href="">Cold and flu</a></li>
                                <li><a href="">Ears, nose, and throat</a></li>
                                <li><a href="">Eye health</a></li>
                                <li><a href="">Face and mouth</a></li>
                                <li><a href="">Fatigue</a></li>
                                <li><a href="">General health</a></li>
                                <li><a href="">Headaches</a></li>
                                <li><a href="">Infectious disease</a></li>
                                <li><a href="">Mental health</a></li>
                                <li><a href="">STDs</a></li>
                                <li><a href="">Women's health</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="getstarted">
        <div class="getstarted__content text-center">
            <p class="title-small">TELEMEDICINE FOR ALL</p>
            <h3 class="title-white">Gain a highly experienced perspective today</h3>
            <a href="/register/" class="btn btn-border-white-large">Get Started</a>
        </div>
    </section>

    <section class="testimonials">
        <div class="testimonials__content container-vertical">
            <div class="testimonials__title text-center">
                <p class="title-small">TESTIMONIALS</p>
                <h2 class="title-large">So many people <br class="d-block d-md-none"> couldn't be wrong</h2>
            </div>
            <div class="testimonials__slide">
                <div class="testimonials__item">
                    <div class="testimonials__item-wrapper">
                        <div>
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 5.png" alt="quote" />
                        </div>
                        <div class="wrapper-description">
                            <p class="description">
                                "Donec quam nunc, accumsan sed enim viverra, semper ultrices nulla. Praesent vestibulum"
                            </p>
                            <p class="name">Bob G </p>
                        </div>
                    </div>
                </div>
                <div class="testimonials__item">
                    <div class="testimonials__item-wrapper">
                        <div>
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 5.png" alt="quote" />
                        </div>
                        <div class="wrapper-description">
                            <p class="description">
                                "Donec quam nunc, accumsan sed enim viverra, semper ultrices nulla. Praesent vestibulum"
                            </p>
                            <p class="name">Bob A</p>
                        </div>
                    </div>
                </div>
                <div class="testimonials__item">
                    <div class="testimonials__item-wrapper">
                        <div>
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 5.png" alt="quote" />
                        </div>
                        <div class="wrapper-description">
                            <p class="description">
                                "Donec quam nunc, accumsan sed enim viverra, semper ultrices nulla. Praesent vestibulum"
                            </p>
                            <p class="name">Bob B</p>
                        </div>
                    </div>
                </div>
                <div class="testimonials__item">
                    <div class="testimonials__item-wrapper">
                        <div>
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 5.png" alt="quote" />
                        </div>
                        <div class="wrapper-description">
                            <p class="description">
                                "Donec quam nunc, accumsan sed enim viverra, semper ultrices nulla. Praesent vestibulum"
                            </p>
                            <p class="name">Bob D</p>
                        </div>
                    </div>
                </div>
                <div class="testimonials__item">
                    <div class="testimonials__item-wrapper">
                        <div>
                            <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 5.png" alt="quote" />
                        </div>
                        <div class="wrapper-description">
                            <p class="description">
                                "Donec quam nunc, accumsan sed enim viverra, semper ultrices nulla. Praesent vestibulum"
                            </p>
                            <p class="name">Bob E</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <a href="/reviews/" class="btn btn-border-purple-large">Reviews</a>
            </div>
        </div>
    </section>

    <section class="feeling">
        <div class="feeling__content d-flex justify-content-center align-items-center">
            <h2 class="feeling__title">How are you feeling today?</h2>
            <a href="/faq/" class="btn btn-border-white-large btn-chat">Chat with a Doctor</a>
        </div>
    </section>

    <section class="industry">
        <div class="industry__content container-vertical">
            <div class="industry__title text-center">
                <p class="title-small">INDUSTRY INSIGHT</p>
                <h2 class="title-large">Health that is real wealth</h2>
            </div>
            <div class="industry__slide">
                <div class="industry__item">
                    <div class="industry__wrapper">
                        <div>
                            <img class="w-100" src="<?php echo get_template_directory_uri()?>/assets/img/NoPath1.jpg" />
                        </div>
                        <p class="date">8.18.2020</p>
                        <div class="wrapper-description">
                            <h4 class="title">Medical Obesity</h4>
                            <p class="description">
                                I believe there is such a thing as Medical Obesity. Obesity is defined as a condition
                                characterized by the excessive accumulation and storage of fat in the body…
                            </p>
                            <a class="btn-read-more">READ MORE ></a>
                        </div>

                    </div>
                </div>
                <div class="industry__item">
                    <div class="industry__wrapper">
                        <div>
                            <img class="w-100" src="<?php echo get_template_directory_uri()?>/assets/img/NoPath2.jpg" />
                        </div>
                        <p class="date">8.18.2020</p>
                        <div class="wrapper-description">
                            <h4 class="title">On Medical Community</h4>
                            <p class="description">
                                Nowadays, we find a tsunami of articles on the epidemics of moral injury and physician
                                burnout. As physicians, we were trained to care for our patients. 99.9%…
                            </p>
                            <a class="btn-read-more">READ MORE ></a>
                        </div>
                    </div>
                </div>
                <div class="industry__item">
                    <div class="industry__wrapper">
                        <div>
                            <img class="w-100" src="<?php echo get_template_directory_uri()?>/assets/img/NoPath3.jpg" />
                        </div>
                        <p class="date">8.18.2020</p>
                        <div class="wrapper-description">
                            <h4 class="title">For The Love of Medicine</h4>
                            <p class="description">
                                This is my response to a recent Newsweek opinion piece written by a seemingly well-
                                respected Yale epidemiologist Dr. Harvey Risch. The SARS CoV2 virus…
                            </p>
                            <a class="btn-read-more">READ MORE ></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <a href="/insight/" class="btn btn-border-purple-large btn-read-more">Read More</a>
            </div>
        </div>
    </section>
</div>

<?php
get_footer();