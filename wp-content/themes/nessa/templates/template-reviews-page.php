<?php
/**
 * Template Name: Reviews Page
 * Template Post Type: page
 */

get_header();
?>

<div class="page-content">
          <section class="reviews">
            <div class="reviews__content container-vertical">
              <div class="reviews__title text-center">
                <p class="title-small">TESTIMONIALS</p>
                <h2 class="title-large">So many people <br class="d-block d-md-none"> couldn't be wrong</h2>
              </div>
              <div class="row">
                  <div class="col-12 col-md-6 col-lg-4">
                    <div class="reviews__item">
                      <div class="reviews__item-wrapper">
                        <div>
                          <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 5.png" alt="quote"/>
                        </div>
                        <div class="wrapper-description">
                          <p class="description">
                            "Donec quam nunc, accumsan sed enim viverra, semper ultrices nulla. Praesent vestibulum"
                          </p>
                          <p class="name">Bob G </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4">
                    <div class="reviews__item">
                      <div class="reviews__item-wrapper">
                        <div>
                          <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 5.png" alt="quote"/>
                        </div>
                        <div class="wrapper-description">
                          <p class="description">
                            "Donec quam nunc, accumsan sed enim viverra, semper ultrices nulla. Praesent vestibulum"
                          </p>
                          <p class="name">Bob A</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4">
                    <div class="reviews__item">
                      <div class="reviews__item-wrapper">
                        <div>
                          <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 5.png" alt="quote"/>
                        </div>
                        <div class="wrapper-description">
                          <p class="description">
                            "Donec quam nunc, accumsan sed enim viverra, semper ultrices nulla. Praesent vestibulum"
                          </p>
                          <p class="name">Bob B</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4">
                    <div class="reviews__item">
                      <div class="reviews__item-wrapper">
                        <div>
                          <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 5.png" alt="quote"/>
                        </div>
                        <div class="wrapper-description">
                          <p class="description">
                            "Donec quam nunc, accumsan sed enim viverra, semper ultrices nulla. Praesent vestibulum"
                          </p>
                          <p class="name">Bob D</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4">
                    <div class="reviews__item">
                      <div class="reviews__item-wrapper">
                        <div>
                          <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 5.png" alt="quote"/>
                        </div>
                        <div class="wrapper-description">
                          <p class="description">
                            "Donec quam nunc, accumsan sed enim viverra, semper ultrices nulla. Praesent vestibulum"
                          </p>
                          <p class="name">Bob E</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4">
                    <div class="reviews__item">
                      <div class="reviews__item-wrapper">
                        <div>
                          <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 5.png" alt="quote"/>
                        </div>
                        <div class="wrapper-description">
                          <p class="description">
                            "Donec quam nunc, accumsan sed enim viverra, semper ultrices nulla. Praesent vestibulum"
                          </p>
                          <p class="name">Bob G </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4">
                    <div class="reviews__item">
                      <div class="reviews__item-wrapper">
                        <div>
                          <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 5.png" alt="quote"/>
                        </div>
                        <div class="wrapper-description">
                          <p class="description">
                            "Donec quam nunc, accumsan sed enim viverra, semper ultrices nulla. Praesent vestibulum"
                          </p>
                          <p class="name">Bob A</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4">
                    <div class="reviews__item">
                      <div class="reviews__item-wrapper">
                        <div>
                          <img src="<?php echo get_template_directory_uri()?>/assets/img/Path 5.png" alt="quote"/>
                        </div>
                        <div class="wrapper-description">
                          <p class="description">
                            "Donec quam nunc, accumsan sed enim viverra, semper ultrices nulla. Praesent vestibulum"
                          </p>
                          <p class="name">Bob B</p>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="text-center wrapper-load-more">
                <button class="btn btn-border-purple-large">LOAD MORE</button>
              </div>
            </div>
          </section>

          <section class="feeling">
            <div class="feeling__content d-flex justify-content-center align-items-center">
              <h2 class="feeling__title">How are you feeling today?</h2>
              <button class="btn btn-border-white-large btn-chat">Chat with a Doctor</button>
            </div>
          </section>
            
          <section class="industry">
            <div class="industry__content container-vertical">
              <div class="industry__title text-center">
                <p class="title-small">INDUSTRY INSIGHT</p>
                <h2 class="title-large">Health that is real wealth</h2>
              </div>
              <div class="industry__slide">
                <div class="industry__item">
                  <div class="industry__wrapper">
                    <div>
                      <img class="w-100" src="<?php echo get_template_directory_uri()?>/assets/img/NoPath1.jpg"/>
                    </div>
                    <p class="date">8.18.2020</p>
                    <div class="wrapper-description">
                      <h4 class="title">Medical Obesity</h4>
                      <p class="description">
                        I believe there is such a thing as Medical Obesity. Obesity is defined as a condition characterized by the excessive accumulation and storage of fat in the body…
                      </p>
                      <a class="btn-read-more">READ MORE ></a>
                    </div>
                    
                  </div>
                </div>
                <div class="industry__item">
                  <div class="industry__wrapper">
                    <div>
                      <img class="w-100" src="<?php echo get_template_directory_uri()?>/assets/img/NoPath2.jpg"/>
                    </div>
                    <p class="date">8.18.2020</p>
                    <div class="wrapper-description">
                      <h4 class="title">On Medical Community</h4>
                      <p class="description">
                        Nowadays, we find a tsunami of articles on the epidemics of moral injury and physician burnout. As physicians, we were trained to care for our patients. 99.9%…
                      </p>
                      <a class="btn-read-more">READ MORE ></a>
                    </div>
                  </div>
                </div>
                <div class="industry__item">
                  <div class="industry__wrapper">
                    <div>
                      <img class="w-100" src="<?php echo get_template_directory_uri()?>/assets/img/NoPath3.jpg"/>
                    </div>
                    <p class="date">8.18.2020</p>
                    <div class="wrapper-description">
                      <h4 class="title">For The Love of Medicine</h4>
                      <p class="description">
                        This is my response to a recent Newsweek opinion piece written by a seemingly well- respected Yale epidemiologist Dr. Harvey Risch. The SARS CoV2 virus…
                      </p>
                      <a class="btn-read-more">READ MORE ></a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="text-center">
                <button class="btn btn-border-purple-large btn-read-more">Read More</button>
              </div>
            </div>
          </section>
        </div>

<?php
get_footer();