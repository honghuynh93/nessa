<?php
/**
 * Template Name: SignIn Page
 * Template Post Type: page
 */

get_header();

global $wpdb, $user_ID;  
if (!$user_ID) { 
   //All code goes in here.  
    if (isset($_POST['user_login']))
    {
        if ( $_POST['action'] == 'log-in' ) {
            # Submit the user login inputs
            $login = wp_login( $_POST['email'], $_POST['password'] );
            $login = wp_signon( array( 'user_login' => $_POST['email'], 'user_password' => $_POST['password'], 'remember' => $_POST['remember-me'] ), false );

    # Redirect to account page after successful login.
            if ( $login->ID ) {
                wp_redirect( home_url('/') );      
            }  
        }
    }
    ?>

    <div class="page-content">
        <section class="signin">
            <div class="signin__content">
                <div class="signin__title">
                    <h4 class="title">Sign in to your account</h4>
                </div>
                <form class="signin__form" name="user_login" action="" method="post" autocomplete="off">
                    <div class="form-group signin__form__item">
                        <img src="<?php echo get_template_directory_uri()?>/assets/img/email icon.svg" />
                        <input type="email" name="email" class="form-control" required data-parsley-trigger="input" autocomplete="off">
                        <label>Email Address</label>
                    </div>
                    <div class="form-group signin__form__item" id="signin_show_hide_password">
                        <img src="<?php echo get_template_directory_uri()?>/assets/img/pass icon.svg" />
                        <input type="password" name="password" class="form-control" required data-parsley-trigger="input" autocomplete="new-password">
                        <i class="fa fa-eye-slash" aria-hidden="true"></i>
                        <label>Password</label>
                    </div>

                    <p class="text-forgot">Forgot password?</p>
                    <button type="submit" class="btn btn-login" name="user_login">LOGIN</button>
                    <input type="hidden" name="action" value="log-in" />
                </form>
                <div class="group-text">
                    <p class="text-account-signin">
                        Don't have an account yet?
                        <a href="/register/">Create one.</a>
                    </p>
                </div>
            </div>
        </section>
    </div>

    <?php

}  
else {  
 wp_redirect( home_url() ); exit;  
}

get_footer();