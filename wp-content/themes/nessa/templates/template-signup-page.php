<?php
/**
 * Template Name: SignUp Page
 * Template Post Type: page
 */

get_header();

global $wpdb, $user_ID;  
if (!$user_ID) {  
   //All code goes in here.  

if (isset($_POST['user_registeration']))
{
    //registration_validation($_POST['username'], $_POST['useremail']);
    global $reg_errors;
    $reg_errors = new WP_Error;
    $first_name=$_POST['first_name'];
    $last_name=$_POST['last_name'];
    $date_of_birth=$_POST['date_of_birth'];
    $username=$_POST['useremail'];
    $useremail=$_POST['useremail'];
    $password=$_POST['password'];
    $confirm_password=$_POST['confirm_password'];
    $signUpError = "";
    
    if(empty( $username ) || empty( $useremail ) || empty($password))
    {
        $reg_errors->add('field', 'Required form field is missing');
    }    
    if ( 6 > strlen( $username ) )
    {
        $reg_errors->add('username_length', 'Username too short. At least 6 characters is required' );
    }
    if ( username_exists( $username ) )
    {
        $reg_errors->add('user_name', 'The username you entered already exists!');
    }
    if ( ! validate_username( $username ) )
    {
        $reg_errors->add( 'username_invalid', 'The username you entered is not valid!' );
    }
    if ( !is_email( $useremail ) )
    {
        $reg_errors->add( 'email_invalid', 'Email id is not valid!' );
    }
    
    if ( email_exists( $useremail ) )
    {
        $reg_errors->add( 'email', 'Email Already exist!' );
    }
    if ( 5 > strlen( $password ) ) {
        $reg_errors->add( 'password', 'Password length must be greater than 5!' );
    }

    if ( $password != $confirm_password ) {
        $reg_errors->add( 'password', 'Confirm Password does not match' );
    }

    if (is_wp_error( $reg_errors ))
    { 
        foreach ( $reg_errors->get_error_messages() as $error )
        {
             $signUpError .= '<p style="color:#FF0000; text-aling:left;"><strong>ERROR</strong>: '.$error . '<br /></p>';
        } 
        echo $signUpError;
    }
    
    // var_dump("<pre>", $reg_errors);

    if ( 1 > count( $reg_errors->get_error_messages() ) )
    {

        // sanitize user form input
        global $username, $useremail;
        $username   =   sanitize_user( $_POST['useremail'] );
        $useremail  =   sanitize_email( $_POST['useremail'] );
        $password   =   esc_attr( $_POST['password'] );
        
        $userdata = array(
            'user_login'    =>   trim($username),
            'user_email'    =>   trim($useremail),
            'user_pass'     =>   trim($password)
            );
        $user_id = wp_insert_user($userdata);  
        // update_user_meta( $user_id, 'user_first_name', trim($first_name));
        // update_user_meta( $user_id, 'user_last_name', trim($last_name));
        // update_user_meta( $user_id, 'user_date_of_birth', trim($date_of_birth));
        wp_redirect( home_url("/login") ); exit; 
    }

}

?>

<div class="page-content">
    <section class="signup">
        <div class="signup__content">
            <div class="signup__title">
                <h4 class="title">Welcome!</h4>
                <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
            </div>
            <form class="signup__form" name="user_registeration" action="" method="post" autocomplete="off">
                <div class="form-group signup__form__item">
                    <input type="text" name="first_name" class="form-control" required data-parsley-trigger="input" autocomplete="off">
                    <label>First Name</label>
                </div>
                <div class="form-group signup__form__item">
                    <input type="text" name="last_name" class="form-control" required data-parsley-trigger="input" autocomplete="off">
                    <label>Last Name</label>
                </div>
                <div class="form-group signup__form__item">
                    <input type="date" name="date_of_birth" class="form-control" required data-parsley-trigger="input" autocomplete="off">
                    <label>Date of Birth</label>
                </div>
                <div class="form-group signup__form__item">
                    <input type="email" name="useremail" class="form-control" required data-parsley-trigger="input" autocomplete="off">
                    <label>Email Address</label>
                </div>
                <div class="form-group signup__form__item" id="signup_show_hide_password">
                    <input type="password" name="password" class="form-control" id="signup-password" required data-parsley-trigger="input" data-parsley-min="6" autocomplete="new-password">
                    <i class="fa fa-eye-slash" aria-hidden="true"></i>
                    <label>Password</label>

                </div>
                <div class="form-group signup__form__item" id="signup_show_hide_confirm_password">
                    <input type="password" name="confirm_password" class="form-control" required data-parsley-trigger="input" data-parsley-equalto="#signup-password" autocomplete="off">
                    <i class="fa fa-eye-slash" aria-hidden="true"></i>
                    <label>Confirm Password</label>
                </div>

                <button type="submit" class="btn btn-next" name="user_registeration">NEXT</button>
            </form>
            <div class="group-text">
                <p class="text-account-signup">
                    Already have an account?
                    <a href="/login/">Sign in</a>
                </p>
                <p class="text-client-signup">Are you a Client?
                    <a href="/login/">Sign in</a>
                </p>
            </div>
        </div>
    </section>
</div>

<?php
}  
else {  
   wp_redirect( home_url() ); exit;  
}

get_footer();